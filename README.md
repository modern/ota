
##  **OTA功能使用说明** 

1.
 **配置 ModernMod** 
> 
请开发者在build.prop中加入以下内容

```
#
# ADD By Modernmod
#
ro.modernmod.version=当前版本
ro.modernmod.devices=机型 （例:Modern Phone）
ro.modernmod.releasetime=发布时间
ro.modernmod.maintainer=你的名字
#可不添加微博
ro.modernmod.maintainer.weibo=你的微博地址
#

```


2.
 **创建分支** 
> 
请开发者在 [modern/ota](http://git.oschina.net/modern/ota) 新建自己机型的分支

> 
分支名称必须为你的 [ModernMod](http://www.modernmod.cn/) 的 build.prop 中的 ```
ro.product.brand
``` 例如我的机型是 HTC M8，那么 build.prop 里面的可能是 htc。

3.
 **创建JSON文件** 
> 
新建完自己的分支之后，请在分支中新建一个后缀为.json的文件

> 
JSON的名称必须为你的 [ModernMod](http://www.modernmod.cn/) 的 build.prop 中的 ```
ro.product.name
``` 例如我的机型是 HTC M8，那么 build.prop 里面的可能是 modern_m8。

4.
 **编辑JSON内容** 
> 
做完上面的一切后你需要在json添加以下内容

> 
```
[{
  "lv": "",
  "sv": "",
  "push": "",
  "rominfo": "",
  "cvchanglog": "",
  "changlog": "",
  "romerinfo": "",
  "developer": ""
}]
```
然后请根据你的实际情况来编写每一项的内容，编写的内容请看下面的注释


**注释** 
```
lv: 这里为最新版本(推送的版本)

sv": 这里为上一个版本，是ota的基础版本，低于这个版本无法进行ota

push: 这里为最新版本的下载链接(必须是直链)

rominfo": 这里是rom的相关信息，如机型，版本之类的

cvchanglog: 这里是历史版本更新日志(也就是基础版本)

changlog: 这里是最新版本更新日志(也就是本次更新的内容)

romerinfo": 这里是维护人员的相关信息

developer: 这里是开发者ID识别
```
 **详细请参考** 
> 
[点击查看模板](http://git.oschina.net/geekupdater/ModernMod-Test/blob/test/test.json)

```

你的机型的 MODERN 需要更新时请修改 JSON 中的 lv，sv，push，rominfo，cvchanglog，changlog，这样才能让推送给用户更新。
```